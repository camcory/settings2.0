package com.android.settings.aoip;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;

import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class AboutUs extends SettingsPreferenceFragment {

    public static final String TAG = "AboutUs";

    Preference mSourceUrl;
    Preference mMikeUrl;
    Preference mTimUrl;
    Preference mCoryUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.about_us);
        mSourceUrl = findPreference("source_aoip");
        mMikeUrl = findPreference("xda_smartguy");
        mTimUrl = findPreference("xda_tim");
        mCoryUrl = findPreference("xda_camcory");

        PreferenceGroup devsGroup = (PreferenceGroup) findPreference("devs");
        ArrayList<Preference> devs = new ArrayList<Preference>();
        for (int i = 0; i < devsGroup.getPreferenceCount(); i++) {
            devs.add(devsGroup.getPreference(i));
        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mSourceUrl) {
            launchUrl("https://bitbucket.org/smartguy044");
        } else if (preference == mMikeUrl) {
            launchUrl("http://forum.xda-developers.com/member.php?u=4525591");
        } else if (preference == mTimUrl) {
            launchUrl("http://forum.xda-developers.com/member.php?u=4555084");
        } else if (preference == mCoryUrl) {
            launchUrl("http://forum.xda-developers.com/member.php?u=4450173");
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void launchUrl(String url) {
        Uri uriUrl = Uri.parse(url);
        Intent web = new Intent(Intent.ACTION_VIEW, uriUrl);
        getActivity().startActivity(web);
    }
}
